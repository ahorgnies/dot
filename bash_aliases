#!/bin/bash

##########
# apt
##########
alias AGI='sudo apt update && sudo apt install'
alias AGU='sudo apt update && sudo apt dist-upgrade'
alias agu='sudo apt update'
alias apm='comm -23 <(apt-mark showmanual | sort -u) <(gzip -dc /var/log/installer/initial-status.gz | sed -n "s/^Package: //p" | sort -u)'

##########
# docker
##########
alias cdd='cd `git rev-parse --show-toplevel`"/src/main/docker"'
alias di='docker inspect'
alias dp='docker ps'
alias drall='docker system prune -a'
alias drc='docker rm `docker ps -qa`'
alias dri='docker rmi -f `docker image ls -q`'
alias drv='docker volume rm `docker volume ls -q`'
alias drn='docker network rm `docker network ls -q`'

##########
# docker-compose
##########
alias dcu='docker-compose up -d'
alias dcd='docker-compose down'
alias dcr='docker-compose up -d --no-deps'

##########
# git
##########
alias cg='cd `git rev-parse --show-toplevel`'
alias gli='git ls-files --others --exclude-standard'

##########
# jhipster
##########
alias djhi='docker container run --name jhipster -v `realpath .`:/home/jhipster/app -v ~/.m2:/home/jhipster/.m2 -p 8080:8080 -p 9000:9000 -p 3001:3001 -d -t jhipster/jhipster'

##########
# kubectl
##########
alias kuba='kubectl get pods --sort-by=.metadata.creationTimestamp'

##########
# ls
##########
alias lsym='ll --color=always | grep --color=never -e "^l.*"'

##########
# neovim
##########
alias vi='nvim'
alias vim='nvim'

##########
# npm
##########
alias ni='npm install'
alias nid='npm install --save-dev'

##########
# rm
##########
alias nuke='sudo rm -rf'

##########
# shell
##########
if [ $SHELL = '/bin/zsh' ]; then
	alias -g C='| wc -l '
	alias -g L='| less'
	alias -g G='| grep '
fi
alias e='"${EDITOR:-nano}"'
alias edit='"${EDITOR:-nano}"'
alias hisg="less $HISTORY | grep"
alias please='sudo'
alias trash='gio trash'

##########
# tar
##########
alias tarbz='tar jcvf'
alias targz='tar zcvf'
alias untarbz='tar jxvf'
alias untargz='tar zxvf'
alias untarxz='tar Jxvf'

##########
# transmision
##########
alias td='transmission-daemon'
alias tdr='transmission-remote'

##########
# miscellanious
##########
alias pyv='source venv/bin/activate'

