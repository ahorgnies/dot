#!/usr/bin/env python3
from subprocess import check_output, call


def is_connected(device_mac):
    return b'Connected: yes\n' in check_output(['bluetoothctl', 'info', device_mac])

def connect(device_mac):
    print('connecting to bluetooth device {}'.format(device_mac))
    call(['bluetoothctl', 'connect', device_mac])

def disconnect(device_mac):
    print('disconnecting from bluetooth device {}'.format(device_mac))
    call(['bluetoothctl', 'disconnect', device_mac])

def main(device_mac):
    if is_connected(device_mac):
        disconnect(device_mac)
    else:
        connect(device_mac)

if __name__ == '__main__':
    main('04:5D:4B:E9:29:D4')

