call plug#begin('~/.config/nvim/plugins')
	Plug 'ervandew/supertab'
call plug#end()

" bash aliases with integrated command line
let $BASH_ENV = "~/.bash_aliases"

" Mapping keys
let mapleader=","
" Allow saving a file when you forgot to open it as super user
cmap w!! w !sudo tee > /dev/null %

" setting options
filetype plugin on
set clipboard=unnamedplus
set number " Show line numbers
set tabstop=2 " tab character is two spaces wide
set shiftwidth=2 " set indentation to two spaces (see tabstop)
set shiftround " use shiftwidth for '<' and '>' control key
set autoindent " indent new lines
set copyindent " indent copied lines
set backspace=indent,eol,start " enable to remove identation, end of line and start of line with backspace
set showmatch " show matching character [](){}<>
set hlsearch " highlight search matches
set incsearch " show search matches as I type

" Function to vimdiff buffer with original file (with last write being effective)
function! s:DiffWithSaved()
  let filetype=&ft
  diffthis
  vnew | r # | normal! 1Gdd
  diffthis
  exe "setlocal bt=nofile bh=wipe nobl noswf ro ft=" . filetype
endfunction
com! DiffSaved call s:DiffWithSaved()

