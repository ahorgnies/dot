export PATH="$PATH":/snap/bin
export EDITOR='nvim'
export HISTORY='~/.bash_history'
# add user scripts to PATH
export PATH=$PATH:/home/aho/.local/bin

# tilix terminal setup
if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
        source /etc/profile.d/vte.sh
fi

